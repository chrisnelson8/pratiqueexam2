package com.example.pratiqueexamen2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView = findViewById(R.id.nav_view);
        bottomNavigationView.setOnClickListener(view -> {
                switch (view.getId()){
                    case R.id.navCalc:
                        setCurrentFragment(new FragmentCalcul());
                        break;
                    case R.id.navPref:
                        setCurrentFragment(new FragmentPreferences());
                        break;
                }
            });
    }

    private void setCurrentFragment(Fragment frag) {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.nav_host_fragment_activity_main, frag)
                .commit();
    }



}