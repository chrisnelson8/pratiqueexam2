package com.example.pratique.affaire;

public class Calculateur {
    private float tauxTaxeFederale;
    private float tauxTaxeProvinciale;
    private float tauxPourboire;
    private float montant;

    public Calculateur() {
        this(0.05f);
    }

    public Calculateur(float tauxTaxeFederale) {
        this(tauxTaxeFederale, 0.09975f);
    }

    public Calculateur(
            float tauxTaxeFederale,
            float tauxTaxeProvinciale) {
        this(tauxTaxeFederale, tauxTaxeProvinciale, 0.15f);
    }

    public Calculateur(
            float tauxTaxeFederale,
            float tauxTaxeProvinciale,
            float tauxPourboire) {
        this.tauxTaxeFederale = tauxTaxeFederale;
        this.tauxTaxeProvinciale = tauxTaxeProvinciale;
        this.tauxPourboire = tauxPourboire;
        montant = 0f;
    }

    public float getTauxTaxeFederale() {
        return tauxTaxeFederale;
    }

    public void setTauxTaxeFederale(float tauxTaxeFederale) {
        this.tauxTaxeFederale = tauxTaxeFederale;
    }

    public float getTauxTaxeProvinciale() {
        return tauxTaxeProvinciale;
    }

    public void setTauxTaxeProvinciale(float tauxTaxeProvinciale) {
        this.tauxTaxeProvinciale = tauxTaxeProvinciale;
    }

    public float getTauxPourboire() {
        return tauxPourboire;
    }

    public void setTauxPourboire(float tauxPourboire) {
        this.tauxPourboire = tauxPourboire;
    }

    public float getMontant() {
        return montant;
    }

    public void setMontant(float montant) throws Exception {
        if (montant < 0f || montant > 1e5f)
            throw new Exception("Le montant doit être entre 0 et 100000");
        this.montant = montant;
    }

    public float calculerTaxeFederale() {
        return montant * tauxTaxeFederale;
    }

    public float calculerTaxeProvinciale() {
        return montant * tauxTaxeProvinciale;
    }

    public float calculerPourboire() {
        return montant * tauxPourboire;
    }

    public float calculerMontantTotal() {
        float total = montant
                * (1 + tauxTaxeFederale + tauxTaxeProvinciale + tauxPourboire);
        float entier = (float) Math.floor(total);
        float fraction = total - entier;
        float fractionAjustee = (float) (0.05f * Math.floor(fraction * 20 + 0.5f));
        return entier + fractionAjustee;
    }
}

